import { AppRegistry, Alert } from "react-native";
import React, { useContext, useEffect } from "react";
import { Provider } from "react-redux";
import App from "./App";
import { name as appName } from "./app.json";
import { setHeartBeat, store } from "./store";
import database from "@react-native-firebase/database";
import api from "./src/api/apiAntares";
import axios from "axios";
import { listenerContext } from "./src/context/listener";
// import { json } from "./src/json/json";
const MyHeadlessTask = async () => {
  // useEffect(() => {});
  // const url_post_device1 = `https://platform.antares.id:8443/~/antares-cse/antares-id/HidroponikHore/device2`;
  // axios
  //   .post(
  //     url_post_device1,
  //     {
  //       "m2m:cin": {
  //         con: `{"nilai ph": 6.98986,
  //         "cahaya": 1432}`,
  //       },
  //     },
  //     {
  //       headers: {
  //         "X-M2M-Origin": "f67b544e770ac646:62c01e7b8af63579",
  //         "Content-Type": "application/json;ty=4",
  //         Accept: "application/json",
  //       },
  //     }
  //   )
  //   .then((res) => {
  //     console.log("device1", res);
  //     // database()
  //     //   .ref("error")
  //     //   .child("")
  //     //   .push(feedBack);
  //   })
  //   .catch((e) => {
  //     console.log(e);
  //     // Alert.alert("Please Update Your Data in Antares");
  //   });
  // const url_post_device2 = `https://platform.antares.id:8443/~/antares-cse/antares-id/HidroponikHore/device1`;
  // axios
  //   .post(
  //     url_post_device2,
  //     {
  //       "m2m:cin": {
  //         con: `{
  //         "suhu": 32.2,
  //         "kelembaban": 58,
  //         "nutirisi1": 1866}`,
  //       },
  //     },
  //     {
  //       headers: {
  //         "X-M2M-Origin": "f67b544e770ac646:62c01e7b8af63579",
  //         "Content-Type": "application/json;ty=4",
  //         Accept: "application/json",
  //       },
  //     }
  //   )
  //   .then((res) => {
  //     console.log("device1", res);
  //     database()
  //       .ref("error")
  //       .child("")
  //       .push(feedBack);
  //   })
  //   .catch((e) => {
  //     console.log(e);
  //     // Alert.alert("Please Update Your Data in Antares");
  //   });
  let json = [];
  let feedBack = {};
  let feedBack2 = {};
  database()
    .ref("jenis_tanaman/")
    .on("child_added", (snapshot) => {
      console.log(snapshot.val());
      json.push({
        id: snapshot.val().id,
        label: snapshot.val().label,
        value: snapshot.val().value,
        nutrisi_atas: snapshot.val().nutrisi_atas,
        nutrisi_bawah: snapshot.val().nutrisi_bawah,
        ph_atas: snapshot.val().ph_atas,
        ph_bawah: snapshot.val().ph_bawah,
      });
      json = [...json];
      console.log("json", json);

      console.log("jalan");
      database()
        .ref("tanaman")
        .on("value", function(snapshot) {
          let tanaman = snapshot.val();
          console.log("index", tanaman);
          // console.log("json", json[parseInt(tanaman)].label);

          const url = `https://platform.antares.id:8443/~/antares-cse/antares-id/HidroponikHore/device1/la`;
          axios
            .get(url, {
              headers: {
                "X-M2M-Origin": "f67b544e770ac646:62c01e7b8af63579",
                "Content-Type": "application/json",
                Accept: "application/json",
              },
            })
            .then((res) => {
              console.log(res);
              let nilai = JSON.parse(res.data["m2m:cin"].con);

              const url_device2 = `https://platform.antares.id:8443/~/antares-cse/antares-id/HidroponikHore/device2/la`;
              axios
                .get(url_device2, {
                  headers: {
                    "X-M2M-Origin": "f67b544e770ac646:62c01e7b8af63579",
                    "Content-Type": "application/json",
                    Accept: "application/json",
                  },
                })
                .then((res) => {
                  console.log(res);
                  let nilai2 = JSON.parse(res.data["m2m:cin"].con);
                  let date = new Date();
                  // lefeedBack = {};
                  // let feedBack2 = {};
                  if (nilai.kelembaban && nilai2.cahaya) {
                    database()
                      .ref(`${tanaman}`)
                      .push({ ...nilai, ...nilai2, dates: date.getTime() });
                    //2
                    if (
                      nilai.nutirisi1 < json[parseInt(tanaman)].nutrisi_bawah &&
                      // nilai.nutirisi1 >= json[parseInt(tanaman)].nutrisi_bawah &&
                      (nilai2["nilai ph"] <= json[parseInt(tanaman)].ph_atas &&
                        nilai2["nilai ph"] >=
                          json[parseInt(tanaman)].ph_bawah) &&
                      nilai.kelembaban >= 67 &&
                      nilai2.cahaya >= 15
                    ) {
                      feedBack = {
                        Nut_atas: json[parseInt(tanaman)].nutrisi_atas,
                        Nut_bawah: json[parseInt(tanaman)].nutrisi_bawah,
                        Nutrition_down:
                          json[parseInt(tanaman)].nutrisi_bawah -
                          nilai.nutirisi1,
                        Nutrition_up: 0,
                        Humidity: 0,
                      };
                      feedBack2 = {
                        Asam_atas: json[parseInt(tanaman)].ph_atas,
                        Asam_bawah: json[parseInt(tanaman)].ph_bawah,
                        ph_down: 0,
                        ph_up: 0,
                        light: 0,
                      };
                    }

                    //3
                    else if (
                      nilai.nutirisi1 <= json[parseInt(tanaman)].nutrisi_atas &&
                      nilai.nutirisi1 >=
                        json[parseInt(tanaman)].nutrisi_bawah &&
                      nilai2["nilai ph"] < json[parseInt(tanaman)].ph_bawah &&
                      // nilai2["nilai ph"] >= json[parseInt(tanaman)].ph_bawah &&
                      nilai.kelembaban >= 67 &&
                      nilai2.cahaya >= 15
                    ) {
                      feedBack = {
                        Nut_atas: json[parseInt(tanaman)].nutrisi_atas,
                        Nut_bawah: json[parseInt(tanaman)].nutrisi_bawah,
                        Nutrition_down: 0,
                        Nutrition_up: 0,
                        Humidity: 0,
                      };
                      feedBack2 = {
                        Asam_atas: json[parseInt(tanaman)].ph_atas,
                        Asam_bawah: json[parseInt(tanaman)].ph_bawah,
                        ph_down:
                          json[parseInt(tanaman)].ph_bawah - nilai2["nilai ph"],
                        ph_up: 0,
                        light: 0,
                      };
                    }
                    //4
                    else if (
                      nilai.nutirisi1 <= json[parseInt(tanaman)].nutrisi_atas &&
                      nilai.nutirisi1 >=
                        json[parseInt(tanaman)].nutrisi_bawah &&
                      nilai2["nilai ph"] <= json[parseInt(tanaman)].ph_atas &&
                      nilai2["nilai ph"] >= json[parseInt(tanaman)].ph_bawah &&
                      nilai.kelembaban < 67 &&
                      nilai2.cahaya >= 15
                    ) {
                      feedBack = {
                        Nut_atas: json[parseInt(tanaman)].nutrisi_atas,
                        Nut_bawah: json[parseInt(tanaman)].nutrisi_bawah,
                        Nutrition_down: 0,
                        Nutrition_up: 0,
                        Humidity: 1,
                      };
                      feedBack2 = {
                        Asam_atas: json[parseInt(tanaman)].ph_atas,
                        Asam_bawah: json[parseInt(tanaman)].ph_bawah,
                        ph_down: 0,
                        ph_up: 0,
                        light: 0,
                      };
                    }
                    //5
                    else if (
                      nilai.nutirisi1 <= json[parseInt(tanaman)].nutrisi_atas &&
                      nilai.nutirisi1 >=
                        json[parseInt(tanaman)].nutrisi_bawah &&
                      nilai2["nilai ph"] <= json[parseInt(tanaman)].ph_atas &&
                      nilai2["nilai ph"] >= json[parseInt(tanaman)].ph_bawah &&
                      nilai.kelembaban >= 67 &&
                      nilai2.cahaya < 15
                    ) {
                      feedBack = {
                        Nut_atas: json[parseInt(tanaman)].nutrisi_atas,
                        Nut_bawah: json[parseInt(tanaman)].nutrisi_bawah,
                        Nutrition_down: 0,
                        Nutrition_up: 0,
                        Humidity: 0,
                      };
                      feedBack2 = {
                        Asam_atas: json[parseInt(tanaman)].ph_atas,
                        Asam_bawah: json[parseInt(tanaman)].ph_bawah,
                        ph_down: 0,
                        ph_up: 0,
                        light: 0,
                      };
                    }
                    //6
                    else if (
                      nilai.nutirisi1 >= json[parseInt(tanaman)].nutrisi_atas &&
                      // nilai.nutirisi1 >= json[parseInt(tanaman)].nutrisi_bawah &&
                      nilai2["nilai ph"] <= json[parseInt(tanaman)].ph_atas &&
                      nilai2["nilai ph"] >= json[parseInt(tanaman)].ph_bawah &&
                      nilai.kelembaban >= 67 &&
                      nilai2.cahaya >= 15
                    ) {
                      feedBack = {
                        Nut_atas: json[parseInt(tanaman)].nutrisi_atas,
                        Nut_bawah: json[parseInt(tanaman)].nutrisi_bawah,
                        Nutrition_up:
                          nilai.nutirisi1 -
                          json[parseInt(tanaman)].nutrisi_atas,
                        Nutrition_down: 0,
                        Humidity: 0,
                      };
                      feedBack2 = {
                        Asam_atas: json[parseInt(tanaman)].ph_atas,
                        Asam_bawah: json[parseInt(tanaman)].ph_bawah,
                        ph_down: 0,
                        ph_up: 0,
                        light: 0,
                      };
                    }
                    //7
                    else if (
                      nilai.nutirisi1 <= json[parseInt(tanaman)].nutrisi_atas &&
                      nilai.nutirisi1 >=
                        json[parseInt(tanaman)].nutrisi_bawah &&
                      nilai2["nilai ph"] > json[parseInt(tanaman)].ph_atas &&
                      // nilai2["nilai ph"] < json[parseInt(tanaman)].ph_bawah &&
                      nilai.kelembaban >= 67 &&
                      nilai2.cahaya >= 15
                    ) {
                      feedBack = {
                        Nut_atas: json[parseInt(tanaman)].nutrisi_atas,
                        Nut_bawah: json[parseInt(tanaman)].nutrisi_bawah,
                        Nutrition_up: 0,
                        Nutrition_down: 0,
                        Humidity: 0,
                      };
                      feedBack2 = {
                        Asam_atas: json[parseInt(tanaman)].ph_atas,
                        Asam_bawah: json[parseInt(tanaman)].ph_bawah,
                        ph_down: 0,
                        ph_up:
                          nilai2["nilai ph"] - json[parseInt(tanaman)].ph_atas,
                        light: 0,
                      };
                    }
                    //8
                    else if (
                      nilai.nutirisi1 < json[parseInt(tanaman)].nutrisi_bawah &&
                      // nilai.nutirisi1 >= json[parseInt(tanaman)].nutrisi_bawah &&
                      nilai2["nilai ph"] < json[parseInt(tanaman)].ph_bawah &&
                      // nilai2["nilai ph"] < json[parseInt(tanaman)].ph_bawah &&
                      nilai.kelembaban >= 67 &&
                      nilai2.cahaya >= 15
                    ) {
                      feedBack = {
                        Nut_atas: json[parseInt(tanaman)].nutrisi_atas,
                        Nut_bawah: json[parseInt(tanaman)].nutrisi_bawah,
                        Nutrition_up: 0,
                        Nutrition_down:
                          json[parseInt(tanaman)].nutrisi_bawah -
                          nilai.nutirisi1,
                        Humidity: 0,
                      };
                      feedBack2 = {
                        Asam_atas: json[parseInt(tanaman)].ph_atas,
                        Asam_bawah: json[parseInt(tanaman)].ph_bawah,
                        ph_down:
                          json[parseInt(tanaman)].ph_bawah - nilai2["nilai ph"],
                        ph_up: 0,
                        light: 0,
                      };
                    }
                    //9
                    else if (
                      nilai.nutirisi1 < json[parseInt(tanaman)].nutrisi_bawah &&
                      // nilai.nutirisi1 >= json[parseInt(tanaman)].nutrisi_bawah &&
                      nilai2["nilai ph"] <= json[parseInt(tanaman)].ph_atas &&
                      nilai2["nilai ph"] >= json[parseInt(tanaman)].ph_bawah &&
                      nilai.kelembaban >= 67 &&
                      nilai2.cahaya >= 15
                    ) {
                      feedBack = {
                        Nut_atas: json[parseInt(tanaman)].nutrisi_atas,
                        Nut_bawah: json[parseInt(tanaman)].nutrisi_bawah,
                        Nutrition_up: 0,
                        Nutrition_down:
                          json[parseInt(tanaman)].nutrisi_bawah -
                          nilai.nutirisi1,
                        Humidity: 1,
                      };
                      feedBack2 = {
                        Asam_atas: json[parseInt(tanaman)].ph_atas,
                        Asam_bawah: json[parseInt(tanaman)].ph_bawah,
                        ph_down: 0,
                        ph_up: 0,
                        light: 0,
                      };
                    }
                    //10
                    else if (
                      nilai.nutirisi1 < json[parseInt(tanaman)].nutrisi_bawah &&
                      // nilai.nutirisi1 >= json[parseInt(tanaman)].nutrisi_bawah &&
                      nilai2["nilai ph"] <= json[parseInt(tanaman)].ph_atas &&
                      nilai2["nilai ph"] >= json[parseInt(tanaman)].ph_bawah &&
                      nilai.kelembaban >= 67 &&
                      nilai2.cahaya < 15
                    ) {
                      feedBack = {
                        Nut_atas: json[parseInt(tanaman)].nutrisi_atas,
                        Nut_bawah: json[parseInt(tanaman)].nutrisi_bawah,
                        Nutrition_up: 0,
                        Nutrition_down:
                          json[parseInt(tanaman)].nutrisi_bawah -
                          nilai.nutirisi1,
                        Humidity: 0,
                      };
                      feedBack2 = {
                        Asam_atas: json[parseInt(tanaman)].ph_atas,
                        Asam_bawah: json[parseInt(tanaman)].ph_bawah,
                        ph_down: 0,
                        ph_up: 0,
                        light: 1,
                      };
                    }
                    //11
                    else if (
                      nilai.nutirisi1 <= json[parseInt(tanaman)].nutrisi_atas &&
                      nilai.nutirisi1 >=
                        json[parseInt(tanaman)].nutrisi_bawah &&
                      // nilai2["nilai ph"] <= json[parseInt(tanaman)].ph_atas &&
                      nilai2["nilai ph"] < json[parseInt(tanaman)].ph_bawah &&
                      nilai.kelembaban < 67 &&
                      nilai2.cahaya >= 0
                    ) {
                      feedBack = {
                        Nut_atas: json[parseInt(tanaman)].nutrisi_atas,
                        Nut_bawah: json[parseInt(tanaman)].nutrisi_bawah,
                        Nutrition_up: 0,
                        Nutrition_down: 0,
                        Humidity: 1,
                      };
                      feedBack2 = {
                        Asam_atas: json[parseInt(tanaman)].ph_atas,
                        Asam_bawah: json[parseInt(tanaman)].ph_bawah,
                        ph_down:
                          json[parseInt(tanaman)].ph_bawah - nilai2["nilai ph"],
                        ph_up: 0,
                        light: 0,
                      };
                    }
                    //12
                    else if (
                      nilai.nutirisi1 <= json[parseInt(tanaman)].nutrisi_atas &&
                      nilai.nutirisi1 >=
                        json[parseInt(tanaman)].nutrisi_bawah &&
                      // nilai2["nilai ph"] <= json[parseInt(tanaman)].ph_atas &&
                      nilai2["nilai ph"] < json[parseInt(tanaman)].ph_bawah &&
                      nilai.kelembaban >= 67 &&
                      nilai2.cahaya < 15
                    ) {
                      feedBack = {
                        Nut_atas: json[parseInt(tanaman)].nutrisi_atas,
                        Nut_bawah: json[parseInt(tanaman)].nutrisi_bawah,
                        Nutrition_up: 0,
                        Nutrition_down: 0,
                        Humidity: 0,
                      };
                      feedBack2 = {
                        Asam_atas: json[parseInt(tanaman)].ph_atas,
                        Asam_bawah: json[parseInt(tanaman)].ph_bawah,
                        ph_down:
                          json[parseInt(tanaman)].ph_bawah - nilai2["nilai ph"],
                        ph_up: 0,
                        light: 1,
                      };
                    }
                    //13
                    else if (
                      nilai.nutirisi1 <= json[parseInt(tanaman)].nutrisi_atas &&
                      nilai.nutirisi1 >=
                        json[parseInt(tanaman)].nutrisi_bawah &&
                      nilai2["nilai ph"] <= json[parseInt(tanaman)].ph_atas &&
                      nilai2["nilai ph"] >= json[parseInt(tanaman)].ph_bawah &&
                      nilai.kelembaban < 67 &&
                      nilai2.cahaya < 15
                    ) {
                      feedBack = {
                        Nut_atas: json[parseInt(tanaman)].nutrisi_atas,
                        Nut_bawah: json[parseInt(tanaman)].nutrisi_bawah,
                        Nutrition_up: 0,
                        Nutrition_down: 0,
                        Humidity: 1,
                      };
                      feedBack2 = {
                        Asam_atas: json[parseInt(tanaman)].ph_atas,
                        Asam_bawah: json[parseInt(tanaman)].ph_bawah,
                        ph_down: 0,
                        ph_up: 0,
                        light: 1,
                      };
                    }
                    //14
                    else if (
                      nilai.nutirisi1 > json[parseInt(tanaman)].nutrisi_atas &&
                      // nilai.nutirisi1 >= json[parseInt(tanaman)].nutrisi_bawah &&
                      nilai2["nilai ph"] > json[parseInt(tanaman)].ph_atas &&
                      // nilai2["nilai ph"] >= json[parseInt(tanaman)].ph_bawah &&
                      nilai.kelembaban >= 67 &&
                      nilai2.cahaya >= 15
                    ) {
                      feedBack = {
                        Nut_atas: json[parseInt(tanaman)].nutrisi_atas,
                        Nut_bawah: json[parseInt(tanaman)].nutrisi_bawah,
                        Nutrition_up:
                          nilai.nutirisi1 -
                          json[parseInt(tanaman)].nutrisi_atas,
                        Nutrition_down: 0,
                        Humidity: 0,
                      };
                      feedBack2 = {
                        Asam_atas: json[parseInt(tanaman)].ph_atas,
                        Asam_bawah: json[parseInt(tanaman)].ph_bawah,
                        ph_down: 0,
                        ph_up:
                          nilai2["nilai ph"] - json[parseInt(tanaman)].ph_atas,
                        light: 1,
                      };
                    }
                    //15
                    else if (
                      // nilai.nutirisi1 > json[parseInt(tanaman)].nutrisi_atas &&
                      nilai.nutirisi1 < json[parseInt(tanaman)].nutrisi_bawah &&
                      nilai2["nilai ph"] > json[parseInt(tanaman)].ph_atas &&
                      // nilai2["nilai ph"] >= json[parseInt(tanaman)].ph_bawah &&
                      nilai.kelembaban >= 67 &&
                      nilai2.cahaya >= 15
                    ) {
                      feedBack = {
                        Nut_atas: json[parseInt(tanaman)].nutrisi_atas,
                        Nut_bawah: json[parseInt(tanaman)].nutrisi_bawah,
                        Nutrition_up: 0,
                        Nutrition_down:
                          json[parseInt(tanaman)].nutrisi_bawah -
                          nilai.nutirisi1,
                        Humidity: 0,
                      };
                      feedBack2 = {
                        Asam_atas: json[parseInt(tanaman)].ph_atas,
                        Asam_bawah: json[parseInt(tanaman)].ph_bawah,
                        ph_down: 0,
                        ph_up:
                          nilai2["nilai ph"] - json[parseInt(tanaman)].ph_atas,
                        light: 0,
                      };
                    }
                    //16
                    else if (
                      nilai.nutirisi1 > json[parseInt(tanaman)].nutrisi_atas &&
                      // nilai.nutirisi1 < json[parseInt(tanaman)].nutrisi_bawah &&
                      // nilai2["nilai ph"] > json[parseInt(tanaman)].ph_atas &&
                      nilai2["nilai ph"] < json[parseInt(tanaman)].ph_bawah &&
                      nilai.kelembaban >= 67 &&
                      nilai2.cahaya >= 15
                    ) {
                      feedBack = {
                        Nut_atas: json[parseInt(tanaman)].nutrisi_atas,
                        Nut_bawah: json[parseInt(tanaman)].nutrisi_bawah,
                        Nutrition_up:
                          nilai.nutirisi1 -
                          json[parseInt(tanaman)].nutrisi_atas,
                        Nutrition_down: 0,
                        Humidity: 0,
                      };
                      feedBack2 = {
                        Asam_atas: json[parseInt(tanaman)].ph_atas,
                        Asam_bawah: json[parseInt(tanaman)].ph_bawah,
                        ph_down:
                          json[parseInt(tanaman)].ph_bawah - nilai2["nilai ph"],
                        ph_up: 0,
                        light: 0,
                      };
                    }
                    //17
                    else if (
                      nilai.nutirisi1 <= json[parseInt(tanaman)].nutrisi_atas &&
                      nilai.nutirisi1 >=
                        json[parseInt(tanaman)].nutrisi_bawah &&
                      nilai2["nilai ph"] > json[parseInt(tanaman)].ph_atas &&
                      // nilai2["nilai ph"] < json[parseInt(tanaman)].ph_bawah &&
                      nilai.kelembaban < 67 &&
                      nilai2.cahaya >= 15
                    ) {
                      feedBack = {
                        Nut_atas: json[parseInt(tanaman)].nutrisi_atas,
                        Nut_bawah: json[parseInt(tanaman)].nutrisi_bawah,
                        Nutrition_up: 0,
                        Nutrition_down: 0,
                        Humidity: 1,
                      };
                      feedBack2 = {
                        Asam_atas: json[parseInt(tanaman)].ph_atas,
                        Asam_bawah: json[parseInt(tanaman)].ph_bawah,
                        ph_down: 0,
                        ph_up:
                          nilai2["nilai ph"] - json[parseInt(tanaman)].ph_atas,
                        light: 0,
                      };
                    }
                    //18
                    else if (
                      nilai.nutirisi1 <= json[parseInt(tanaman)].nutrisi_atas &&
                      nilai.nutirisi1 >=
                        json[parseInt(tanaman)].nutrisi_bawah &&
                      nilai2["nilai ph"] > json[parseInt(tanaman)].ph_atas &&
                      // nilai2["nilai ph"] < json[parseInt(tanaman)].ph_bawah &&
                      nilai.kelembaban >= 67 &&
                      nilai2.cahaya < 15
                    ) {
                      feedBack = {
                        Nut_atas: json[parseInt(tanaman)].nutrisi_atas,
                        Nut_bawah: json[parseInt(tanaman)].nutrisi_bawah,
                        Nutrition_up: 0,
                        Nutrition_down: 0,
                        Humidity: 0,
                      };
                      feedBack2 = {
                        Asam_atas: json[parseInt(tanaman)].ph_atas,
                        Asam_bawah: json[parseInt(tanaman)].ph_bawah,
                        ph_down: 0,
                        ph_up:
                          nilai2["nilai ph"] - json[parseInt(tanaman)].ph_atas,
                        light: 1,
                      };
                    }
                    //19
                    else if (
                      nilai.nutirisi1 > json[parseInt(tanaman)].nutrisi_atas &&
                      // nilai.nutirisi1 >= json[parseInt(tanaman)].nutrisi_bawah &&
                      nilai2["nilai ph"] <= json[parseInt(tanaman)].ph_atas &&
                      nilai2["nilai ph"] >= json[parseInt(tanaman)].ph_bawah &&
                      nilai.kelembaban < 67 &&
                      nilai2.cahaya >= 15
                    ) {
                      feedBack = {
                        Nut_atas: json[parseInt(tanaman)].nutrisi_atas,
                        Nut_bawah: json[parseInt(tanaman)].nutrisi_bawah,
                        Nutrition_up: 0,
                        Nutrition_down: 0,
                        Humidity: 1,
                      };
                      feedBack2 = {
                        Asam_atas: json[parseInt(tanaman)].ph_atas,
                        Asam_bawah: json[parseInt(tanaman)].ph_bawah,
                        ph_down: 0,
                        ph_up:
                          nilai2["nilai ph"] - json[parseInt(tanaman)].ph_atas,
                        light: 0,
                      };
                    }
                    //20
                    else if (
                      nilai.nutirisi1 > json[parseInt(tanaman)].nutrisi_atas &&
                      // nilai.nutirisi1 >= json[parseInt(tanaman)].nutrisi_bawah &&
                      nilai2["nilai ph"] <= json[parseInt(tanaman)].ph_atas &&
                      nilai2["nilai ph"] >= json[parseInt(tanaman)].ph_bawah &&
                      nilai.kelembaban >= 67 &&
                      nilai2.cahaya < 15
                    ) {
                      feedBack = {
                        Nut_atas: json[parseInt(tanaman)].nutrisi_atas,
                        Nut_bawah: json[parseInt(tanaman)].nutrisi_bawah,
                        Nutrition_up:
                          nilai.nutirisi1 -
                          json[parseInt(tanaman)].nutrisi_atas,
                        Nutrition_down: 0,
                        Humidity: 0,
                      };
                      feedBack2 = {
                        Asam_atas: json[parseInt(tanaman)].ph_atas,
                        Asam_bawah: json[parseInt(tanaman)].ph_bawah,
                        ph_down: 0,
                        ph_up: 0,
                        light: 1,
                      };
                    }
                    //21
                    else if (
                      // nilai.nutirisi1 > json[parseInt(tanaman)].nutrisi_atas &&
                      nilai.nutirisi1 < json[parseInt(tanaman)].nutrisi_bawah &&
                      // nilai2["nilai ph"] <= json[parseInt(tanaman)].ph_atas &&
                      nilai2["nilai ph"] < json[parseInt(tanaman)].ph_bawah &&
                      nilai.kelembaban < 67 &&
                      nilai2.cahaya >= 15
                    ) {
                      feedBack = {
                        Nut_atas: json[parseInt(tanaman)].nutrisi_atas,
                        Nut_bawah: json[parseInt(tanaman)].nutrisi_bawah,
                        Nutrition_up: 0,
                        Nutrition_down:
                          json[parseInt(tanaman)].nutrisi_bawah -
                          nilai.nutirisi1,
                        Humidity: 1,
                      };
                      feedBack2 = {
                        Asam_atas: json[parseInt(tanaman)].ph_atas,
                        Asam_bawah: json[parseInt(tanaman)].ph_bawah,
                        ph_down:
                          json[parseInt(tanaman)].ph_bawah - nilai2["nilai ph"],
                        ph_up: 0,
                        light: 0,
                      };
                    }
                    //22
                    else if (
                      // nilai.nutirisi1 > json[parseInt(tanaman)].nutrisi_atas &&
                      nilai.nutirisi1 < json[parseInt(tanaman)].nutrisi_bawah &&
                      // nilai2["nilai ph"] <= json[parseInt(tanaman)].ph_atas &&
                      nilai2["nilai ph"] < json[parseInt(tanaman)].ph_bawah &&
                      nilai.kelembaban >= 67 &&
                      nilai2.cahaya < 15
                    ) {
                      feedBack = {
                        Nut_atas: json[parseInt(tanaman)].nutrisi_atas,
                        Nut_bawah: json[parseInt(tanaman)].nutrisi_bawah,
                        Nutrition_up: 0,
                        Nutrition_down:
                          json[parseInt(tanaman)].nutrisi_bawah -
                          nilai.nutirisi1,
                        Humidity: 0,
                      };
                      feedBack2 = {
                        Asam_atas: json[parseInt(tanaman)].ph_atas,
                        Asam_bawah: json[parseInt(tanaman)].ph_bawah,
                        ph_down:
                          json[parseInt(tanaman)].ph_bawah - nilai2["nilai ph"],
                        ph_up: 0,
                        light: 1,
                      };
                    }
                    //23
                    else if (
                      // nilai.nutirisi1 > json[parseInt(tanaman)].nutrisi_atas &&
                      nilai.nutirisi1 < json[parseInt(tanaman)].nutrisi_bawah &&
                      nilai2["nilai ph"] <= json[parseInt(tanaman)].ph_atas &&
                      nilai2["nilai ph"] >= json[parseInt(tanaman)].ph_bawah &&
                      nilai.kelembaban < 67 &&
                      nilai2.cahaya < 15
                    ) {
                      feedBack = {
                        Nut_atas: json[parseInt(tanaman)].nutrisi_atas,
                        Nut_bawah: json[parseInt(tanaman)].nutrisi_bawah,
                        Nutrition_up: 0,
                        Nutrition_down:
                          json[parseInt(tanaman)].nutrisi_bawah -
                          nilai.nutirisi1,
                        Humidity: 1,
                      };
                      feedBack2 = {
                        Asam_atas: json[parseInt(tanaman)].ph_atas,
                        Asam_bawah: json[parseInt(tanaman)].ph_bawah,
                        ph_down: 0,
                        ph_up: 0,
                        light: 1,
                      };
                    }
                    //24
                    else if (
                      nilai.nutirisi1 <= json[parseInt(tanaman)].nutrisi_atas &&
                      nilai.nutirisi1 >=
                        json[parseInt(tanaman)].nutrisi_bawah &&
                      // nilai2["nilai ph"] <= json[parseInt(tanaman)].ph_atas &&
                      nilai2["nilai ph"] < json[parseInt(tanaman)].ph_bawah &&
                      nilai.kelembaban < 67 &&
                      nilai2.cahaya < 15
                    ) {
                      feedBack = {
                        Nut_atas: json[parseInt(tanaman)].nutrisi_atas,
                        Nut_bawah: json[parseInt(tanaman)].nutrisi_bawah,
                        Nutrition_up: 0,
                        Nutrition_down: 0,
                        Humidity: 1,
                      };
                      feedBack2 = {
                        Asam_atas: json[parseInt(tanaman)].ph_atas,
                        Asam_bawah: json[parseInt(tanaman)].ph_bawah,
                        ph_down:
                          json[parseInt(tanaman)].ph_bawah - nilai2["nilai ph"],
                        ph_up: 0,
                        light: 1,
                      };
                    }
                    //25
                    else if (
                      nilai.nutirisi1 > json[parseInt(tanaman)].nutrisi_atas &&
                      // nilai.nutirisi1 >= json[parseInt(tanaman)].nutrisi_bawah &&
                      // nilai2["nilai ph"] <= json[parseInt(tanaman)].ph_atas &&
                      nilai2["nilai ph"] < json[parseInt(tanaman)].ph_bawah &&
                      nilai.kelembaban < 67 &&
                      nilai2.cahaya >= 15
                    ) {
                      feedBack = {
                        Nut_atas: json[parseInt(tanaman)].nutrisi_atas,
                        Nut_bawah: json[parseInt(tanaman)].nutrisi_bawah,
                        Nutrition_up:
                          nilai.nutirisi1 -
                          json[parseInt(tanaman)].nutrisi_atas,
                        Nutrition_down: 0,
                        Humidity: 1,
                      };
                      feedBack2 = {
                        Asam_atas: json[parseInt(tanaman)].ph_atas,
                        Asam_bawah: json[parseInt(tanaman)].ph_bawah,
                        ph_down:
                          json[parseInt(tanaman)].ph_bawah - nilai2["nilai ph"],
                        ph_up: 0,
                        light: 0,
                      };
                    }
                    //26
                    else if (
                      nilai.nutirisi1 > json[parseInt(tanaman)].nutrisi_atas &&
                      // nilai.nutirisi1 >= json[parseInt(tanaman)].nutrisi_bawah &&
                      nilai2["nilai ph"] <= json[parseInt(tanaman)].ph_atas &&
                      nilai2["nilai ph"] >= json[parseInt(tanaman)].ph_bawah &&
                      nilai.kelembaban < 67 &&
                      nilai2.cahaya < 15
                    ) {
                      feedBack = {
                        Nut_atas: json[parseInt(tanaman)].nutrisi_atas,
                        Nut_bawah: json[parseInt(tanaman)].nutrisi_bawah,
                        Nutrition_up:
                          nilai.nutirisi1 -
                          json[parseInt(tanaman)].nutrisi_atas,
                        Nutrition_down: 0,
                        Humidity: 1,
                      };
                      feedBack2 = {
                        Asam_atas: json[parseInt(tanaman)].ph_atas,
                        Asam_bawah: json[parseInt(tanaman)].ph_bawah,
                        ph_down: 0,
                        ph_up: 0,
                        light: 1,
                      };
                    }
                    //27
                    else if (
                      // nilai.nutirisi1 > json[parseInt(tanaman)].nutrisi_atas &&
                      nilai.nutirisi1 < json[parseInt(tanaman)].nutrisi_bawah &&
                      nilai2["nilai ph"] > json[parseInt(tanaman)].ph_atas &&
                      // nilai2["nilai ph"] >= json[parseInt(tanaman)].ph_bawah &&
                      nilai.kelembaban < 67 &&
                      nilai2.cahaya >= 15
                    ) {
                      feedBack = {
                        Nut_atas: json[parseInt(tanaman)].nutrisi_atas,
                        Nut_bawah: json[parseInt(tanaman)].nutrisi_bawah,
                        Nutrition_up: 0,
                        Nutrition_down:
                          json[parseInt(tanaman)].nutrisi_bawah -
                          nilai.nutirisi1,
                        Humidity: 1,
                      };
                      feedBack2 = {
                        Asam_atas: json[parseInt(tanaman)].ph_atas,
                        Asam_bawah: json[parseInt(tanaman)].ph_bawah,
                        ph_down: 0,
                        ph_up:
                          nilai2["nilai ph"] - json[parseInt(tanaman)].ph_atas,
                        light: 0,
                      };
                    }
                    //28
                    else if (
                      nilai.nutirisi1 <= json[parseInt(tanaman)].nutrisi_atas &&
                      nilai.nutirisi1 >=
                        json[parseInt(tanaman)].nutrisi_bawah &&
                      nilai2["nilai ph"] > json[parseInt(tanaman)].ph_atas &&
                      // nilai2["nilai ph"] >= json[parseInt(tanaman)].ph_bawah &&
                      nilai.kelembaban < 67 &&
                      nilai2.cahaya < 15
                    ) {
                      feedBack = {
                        Nut_atas: json[parseInt(tanaman)].nutrisi_atas,
                        Nut_bawah: json[parseInt(tanaman)].nutrisi_bawah,
                        Nutrition_up: 0,
                        Nutrition_down: 0,
                        Humidity: 1,
                      };
                      feedBack2 = {
                        Asam_atas: json[parseInt(tanaman)].ph_atas,
                        Asam_bawah: json[parseInt(tanaman)].ph_bawah,
                        ph_down: 0,
                        ph_up:
                          nilai2["nilai ph"] - json[parseInt(tanaman)].ph_atas,
                        light: 1,
                      };
                    }
                    //29
                    else if (
                      nilai.nutirisi1 > json[parseInt(tanaman)].nutrisi_atas &&
                      // nilai.nutirisi1 >= json[parseInt(tanaman)].nutrisi_bawah &&
                      nilai2["nilai ph"] > json[parseInt(tanaman)].ph_atas &&
                      // nilai2["nilai ph"] >= json[parseInt(tanaman)].ph_bawah &&
                      nilai.kelembaban < 67 &&
                      nilai2.cahaya >= 15
                    ) {
                      feedBack = {
                        Nut_atas: json[parseInt(tanaman)].nutrisi_atas,
                        Nut_bawah: json[parseInt(tanaman)].nutrisi_bawah,
                        Nutrition_up:
                          nilai.nutirisi1 -
                          json[parseInt(tanaman)].nutrisi_atas,
                        Nutrition_down: 0,
                        Humidity: 1,
                      };
                      feedBack2 = {
                        Asam_atas: json[parseInt(tanaman)].ph_atas,
                        Asam_bawah: json[parseInt(tanaman)].ph_bawah,
                        ph_down: 0,
                        ph_up:
                          nilai2["nilai ph"] - json[parseInt(tanaman)].ph_atas,
                        light: 1,
                      };
                    } else {
                      feedBack = {};
                      feedBack2 = {};
                      console.log("oke", nilai, nilai2);
                    }
                    console.log(feedBack, feedBack2);
                    if (feedBack.Nut_atas && feedBack2.Asam_atas) {
                      const url_post_device1 = `https://platform.antares.id:8443/~/antares-cse/antares-id/HidroponikHore/device1`;
                      axios
                        .post(
                          url_post_device1,
                          {
                            "m2m:cin": {
                              con: `{
                          "Nut_atas":${feedBack.Nut_atas},
                          "Nut_bawah":${feedBack.Nut_bawah},
                          "Nutrition_up": ${feedBack.Nutrition_up},
                          "Nutrition_down": ${feedBack.Nutrition_down},
                          "Humadity": ${feedBack.Humidity}}`,
                            },
                          },
                          {
                            headers: {
                              "X-M2M-Origin":
                                "f67b544e770ac646:62c01e7b8af63579",
                              "Content-Type": "application/json;ty=4",
                              Accept: "application/json",
                            },
                          }
                        )
                        .then((res) => {
                          console.log("device1", res);
                        })
                        .catch((e) => {
                          console.log(e);
                        });
                      const url_post_device2 = `https://platform.antares.id:8443/~/antares-cse/antares-id/HidroponikHore/device2`;

                      axios
                        .post(
                          url_post_device2,
                          {
                            "m2m:cin": {
                              con: `{
                        "Asam_atas":${feedBack2.Asam_atas},
                        "Asam_bawah":${feedBack2.Asam_bawah},
                        "ph_down":${feedBack2.ph_down},
                        "ph_up":${feedBack2.ph_up},
                        "light":${feedBack2.light}}`,
                            },
                          },
                          {
                            headers: {
                              "X-M2M-Origin":
                                "f67b544e770ac646:62c01e7b8af63579",
                              "Content-Type": "application/json;ty=4",
                              Accept: "application/json",
                            },
                          }
                        )
                        .then((res) => {
                          console.log("device2", res);
                        })
                        .catch((e) => {
                          console.log(e.message);
                        });
                    }
                  }
                })
                .catch((e) => {
                  console.log(e);
                });
            })
            .catch((e) => {
              console.log("error get", e);
            });
        });
    });
};

const RNRedux = () => (
  <Provider store={store}>
    <App />
  </Provider>
);

AppRegistry.registerHeadlessTask("Heartbeat", () => MyHeadlessTask);
AppRegistry.registerComponent(appName, () => RNRedux);
