const initialState = {
  status: false,
  values: {},
  value: "",
};

export const listenerReducer = (state, action) => {
  switch (action.type) {
    case "ON":
      return {
        ...state,
        status: true,
      };
    case "OFF":
      return {
        ...state,
        status: false,
      };
    case "VALUES":
      return {
        ...state,
        value: "",
      };
    case "VALUE":
      return {
        ...state,
        value: action.value,
      };
    case "RESET_lANG": {
      console.log("action", action.current_lang);
      return action.current_lang;
    }
    default:
      return state;
  }
};
