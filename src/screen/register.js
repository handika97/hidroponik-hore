import React, { useEffect, useContext, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  NativeModules,
  ImageBackground,
  TextInput,
} from "react-native";
import { connect } from "react-redux";
import Heartbeat from "../../Heartbeat";
import database from "@react-native-firebase/database";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { ScrollView } from "react-native-gesture-handler";
import { listenerContext } from "../context/listener";
import { authContext } from "../context/authSlice";

const Register = ({ navigation, heartBeat }) => {
  const { auth, dispatch_auth } = useContext(authContext);
  const [Username, setUsername] = useState("");
  const [Password, setPassword] = useState("");
  const [data, setdata] = useState("");
  console.log(auth);

  const regis = () => {
    if (Username && Password) {
      let index = auth.data?.filter(function(obj) {
        return obj.name.toLowerCase() === Username.toLowerCase();
      });
      if (index.length > 0) {
        alert("Username Sudah Terdaftar");
      } else {
        console.log("index", index);
        dispatch_auth({
          type: "Register",
          name: Username.toUpperCase(),
          data: [
            ...auth.data,
            { name: Username.toLowerCase(), password: Password },
          ],
        });
      }
    } else {
      alert("Username atau Password Tidak Boleh Kosong");
    }
    // auth.data.some;
  };

  return (
    <View style={styles.container}>
      <View style={{ justifyContent: "center", alignItems: "center" }}>
        <View style={{ height: wp(12), width: wp(80) }}>
          <Image
            source={require("../asset/icon.png")}
            style={{ height: "100%", width: "100%" }}
          />
        </View>
        <View
          style={{
            height: 50,
            width: wp(80),
            marginTop: hp(3),
            borderWidth: 1,
            borderRadius: 30,
            borderColor: "black",
            alignItems: "center",
            padding: 5,
            flexDirection: "row",
          }}
        >
          <View
            style={{
              height: 35,
              width: 35,
              position: "absolute",
              marginLeft: wp("2"),
            }}
          >
            <Image
              source={require("../asset/user1.png")}
              style={{ height: "100%", width: "100%" }}
            />
          </View>
          <View style={{ width: "100%", alignItems: "center" }}>
            <TextInput
              placeholder="Username"
              value={Username}
              onChangeText={setUsername}
            />
          </View>
        </View>
        <View
          style={{
            height: 50,
            width: wp(80),
            marginTop: hp(3),
            borderWidth: 1,
            borderRadius: 30,
            borderColor: "black",
            alignItems: "center",
            padding: 5,
            flexDirection: "row",
          }}
        >
          <View
            style={{
              height: 35,
              width: 35,
              position: "absolute",
              marginLeft: wp("2"),
            }}
          >
            <Image
              source={require("../asset/padlock.png")}
              style={{ height: "100%", width: "100%" }}
            />
          </View>
          <View style={{ width: "100%", alignItems: "center" }}>
            <TextInput
              placeholder="Password"
              secureTextEntry
              value={Password}
              onChangeText={setPassword}
            />
          </View>
        </View>
        <TouchableOpacity
          style={{
            height: 40,
            width: wp(33),
            backgroundColor: "#008dee",
            justifyContent: "center",
            alignItems: "center",
            borderRadius: 30,
            marginTop: hp(5),
          }}
          onPress={() => {
            regis();
          }}
        >
          <View>
            <Text style={{ color: "white", fontWeight: "bold", fontSize: 17 }}>
              Register
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "white",
    justifyContent: "center",
  },
  view: {
    flex: 0.5,
    justifyContent: "center",
    alignItems: "center",
  },
  button: {
    backgroundColor: "gray",
    padding: 10,
    margin: 10,
  },
  text: {
    fontSize: 20,
    color: "white",
  },
});

const mapStateToProps = (store) => ({
  heartBeat: store.App.heartBeat,
});

export default connect(mapStateToProps)(Register);
