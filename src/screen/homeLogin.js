import React, { useEffect, useContext, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  NativeModules,
  ImageBackground,
  TextInput,
} from "react-native";
import { connect } from "react-redux";
import Heartbeat from "../../Heartbeat";
import database from "@react-native-firebase/database";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { ScrollView } from "react-native-gesture-handler";
import { listenerContext } from "../context/listener";

const mainMenu = ({ navigation, heartBeat }) => {
  return (
    <View style={styles.container}>
      <View style={{ justifyContent: "center", alignItems: "center" }}>
        <View style={{ height: wp(12), width: wp(80) }}>
          <Image
            source={require("../asset/icon.png")}
            style={{ height: "100%", width: "100%" }}
          />
        </View>

        <TouchableOpacity
          style={{
            height: 43,
            width: wp(50),
            backgroundColor: "#008dee",
            justifyContent: "center",
            alignItems: "center",
            borderRadius: 30,
            marginTop: hp(5),
          }}
          onPress={() => navigation.navigate("login")}
        >
          <Text style={{ color: "white", fontWeight: "bold", fontSize: 20 }}>
            LOGIN
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            height: 43,
            width: wp(50),
            backgroundColor: "#008dee",
            justifyContent: "center",
            alignItems: "center",
            borderRadius: 30,
            marginTop: hp(5),
          }}
          onPress={() => navigation.navigate("register")}
        >
          <Text style={{ color: "white", fontWeight: "bold", fontSize: 20 }}>
            Register
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "white",
    justifyContent: "center",
  },
  view: {
    flex: 0.5,
    justifyContent: "center",
    alignItems: "center",
  },
  button: {
    backgroundColor: "gray",
    padding: 10,
    margin: 10,
  },
  text: {
    fontSize: 20,
    color: "white",
  },
});

const mapStateToProps = (store) => ({
  heartBeat: store.App.heartBeat,
});

export default connect(mapStateToProps)(mainMenu);
