import React, { useEffect, useContext, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  NativeModules,
  Modal,
  ImageBackground,
  TextInput,
  Picker,
} from "react-native";
import { connect } from "react-redux";
import Heartbeat from "../../Heartbeat";
import database from "@react-native-firebase/database";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { setHeartBeat, store } from "../../store";
import { ScrollView } from "react-native-gesture-handler";
import { listenerContext } from "../context/listener";
import { authContext } from "../context/authSlice";
// import { json } from "../json/json";
const choice = ({ navigation, heartBeat }) => {
  const { auth, dispatch_auth } = useContext(authContext);
  const { list, dispatch_listener } = useContext(listenerContext);
  const [Visible, setVisible] = useState(list.status ? false : true);
  const [json, setjson] = useState([]);
  const [selectedValue, setSelectedValue] = useState(0);
  const [value, setValue] = useState({
    nutirisi1: "",
    "nilai ph": "",
    kelembaban: "",
    suhu: "",
    cahaya: "",
  });

  useEffect(() => {
    let data = database().ref("jenis_tanaman/");
    data.on("child_added", (snapshot) => {
      json.push({
        id: snapshot.val().id,
        label: snapshot.val().label,
        value: snapshot.val().value,
        nutrisi_atas: snapshot.val().nutrisi_atas,
        nutrisi_bawah: snapshot.val().nutrisi_bawah,
        ph_atas: snapshot.val().ph_atas,
        ph_bawah: snapshot.val().ph_bawah,
      });
      setjson([...json]);
    });
    // for (let i = 0; i < json.length; i++) {
    //   database()
    //     .ref("jenis_tanaman/")
    //     .push(json[i]);
    // }
    return () => {
      data.off();
    };
  }, []);

  const InputTanaman = () => {
    let index = auth.data?.filter(function(obj) {
      return obj.name.toLowerCase() === auth.name.toLowerCase();
    });
    // console.log(index);
    if (index.length > 0) {
      let New = auth.data;
      for (let i = 0; i < New.length; i++) {
        if (New[i].name.toLowerCase() === auth.name.toLowerCase()) {
          New[i].id = json[selectedValue].id;
          New[i].label = json[selectedValue].label;
          // console.log(New);
        }
      }
      dispatch_auth({
        type: "FORGET_PASSWORD",
        data: New,
      });
    }
    database()
      .ref("tanaman")
      .set(`${json[selectedValue].value}`);
    Heartbeat.startService();
    dispatch_listener({
      type: "ON",
    });
    dispatch_listener({
      type: "VALUE",
      value: json[selectedValue].label,
    });
  };

  return (
    <View style={styles.container}>
      <ImageBackground
        source={require("../asset/backgroundd.png")}
        style={{ flex: 1, resizeMode: "cover" }}
      >
        <View
          style={{
            backgroundColor: "#008dee",
            height: hp(17),
            width: wp(100),
            borderBottomLeftRadius: 30,
            borderBottomRightRadius: 30,
            paddingHorizontal: wp(5),
            paddingVertical: hp(1),
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <View style={{ height: hp(9), width: hp(40) }}>
            <Image
              source={require("../asset/icon.png")}
              style={{ height: "100%", width: "100%" }}
            />
          </View>
        </View>
        <ScrollView>
          <View style={{ alignItems: "center", flexDirection: "column" }}>
            <Text style={{ fontSize: hp(3.5), marginVertical: hp(3) }}>
              MASUKKAN JENIS TANAMAN
            </Text>
            <View style={{ borderBottomColor: "black", borderBottomWidth: 1 }}>
              {json && (
                <Picker
                  selectedValue={selectedValue}
                  style={{
                    height: hp(5),
                    width: wp(70),
                    fontSize: hp(2.5),
                    fontWeight: "bold",
                  }}
                  onValueChange={(itemValue, itemIndex) =>
                    setSelectedValue(itemValue)
                  }
                >
                  {json?.map((item) => (
                    <Picker.Item
                      label={item.label}
                      value={item.value}
                      stylestyle={{
                        fontSize: hp(2.5),
                        fontWeight: "bold",
                      }}
                    />
                  ))}
                </Picker>
              )}
            </View>
            <View
              style={{
                borderBottomColor: "black",
                borderBottomWidth: 1,
                height: hp(5),
                width: wp(70),
                marginVertical: hp(3),
              }}
            >
              <Text style={{ fontSize: hp(2.5), fontWeight: "bold" }}>
                Nutrisi{" "}
                {selectedValue != 0
                  ? `${json[selectedValue].nutrisi_bawah} - ${
                      json[selectedValue].nutrisi_atas
                    }`
                  : null}
              </Text>
            </View>
            <View
              style={{
                borderBottomColor: "black",
                borderBottomWidth: 1,
                height: hp(5),
                width: wp(70),
                marginVertical: hp(3),
              }}
            >
              <Text style={{ fontSize: hp(2.5), fontWeight: "bold" }}>
                pH{" "}
                {selectedValue != 0
                  ? `${json[selectedValue].ph_bawah} - ${
                      json[selectedValue].ph_atas
                    }`
                  : null}
              </Text>
            </View>
          </View>
        </ScrollView>
        <TouchableOpacity
          style={{
            alignItems: "flex-end",
            justifyContent: "flex-end",
            bottom: 0,
            flex: 1,
            height: "100%",
            justifyContent: "flex-end",
            alignItems: "flex-end",
            marginHorizontal: 20,
          }}
          onPress={() => InputTanaman()}
        >
          <View
            style={{
              height: hp(7),
              width: hp(7),
              marginVertical: hp(3),
              position: "absolute",
              bottom: 0,
            }}
          >
            <Image
              source={require("../asset/check.png")}
              style={{ height: "100%", width: "100%" }}
            />
          </View>
        </TouchableOpacity>
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "white",
    alignItems: "center",
  },
  view: {
    flex: 0.5,
    justifyContent: "center",
    alignItems: "center",
  },
  button: {
    backgroundColor: "gray",
    padding: 10,
    margin: 10,
  },
  text: {
    fontSize: 20,
    color: "white",
  },
});

const mapStateToProps = (store) => ({
  heartBeat: store.App.heartBeat,
});

export default connect(mapStateToProps)(choice);
