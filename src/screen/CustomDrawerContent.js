import React, { useEffect, useContext, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  NativeModules,
  ImageBackground,
  SafeAreaView,
  FlatList,
} from "react-native";
import { authContext } from "../context/authSlice";
import { listenerContext } from "../context/listener";
import database from "@react-native-firebase/database";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import Heartbeat from "../../Heartbeat";

const CustomDrawerContent = (props) => {
  const { auth, dispatch_auth } = useContext(authContext);
  const { list, dispatch_listener } = useContext(listenerContext);
  const [value, setValue] = useState([]);
  const [history, sethistory] = useState([]);
  const [tanaman, settanaman] = useState("");
  let { navigation } = props;
  useEffect(() => {
    // let data = null;
    database()
      .ref("tanaman")
      .on("value", function(snapshot) {
        settanaman(snapshot.val());
        // console.log("oke", tanaman, snapshot.val());
        if (tanaman) {
          let data = database().ref(`${tanaman}`);
          data.on("child_added", (snapshot) => {
            if (snapshot.val().nutirisi1) {
              let data = snapshot.val();
              // console.log(snapshot.val()["nilai ph"]);
              value.push({
                nutirisi1: snapshot.val().nutirisi1,
                ph: `${data["nilai ph"]}`,
                kelembaban: snapshot.val().kelembaban,
                suhu: snapshot.val().suhu,
                cahaya: snapshot.val().cahaya,
                date: snapshot.val().dates,
              });
              setValue([...value]);
            }
          });
        }
      });

    return () => {
      if (tanaman) {
        let data = database().ref(`${tanaman}`);
        data.off();
      }
    };
  }, [tanaman]);

  // useEffect(() => {
  //   if (tanaman) {
  //     setValue([]);
  //   }
  // }, [tanaman]);

  useEffect(() => {
    // sethistory([]);
    // let newvalue = value?.reverse();
    // let newValues = [];
    // if (value.length) {
    //   for (let i = 0; i < newvalue.length; i++) {
    //     if (history.length <= 5) {
    //       newValues = newValues.concat(newvalue[i]);
    //       newValues = newValues.reverse();
    //       sethistory(newValues.reverse());
    //     }
    //     newValues = [];
    //   }
    // }
  }, [value]);

  const Times = (time) => {
    let hours = null;
    let minutes = null;
    if (parseInt(new Date(time).getHours()) === 24) {
      hours = "00";
    } else if (parseInt(new Date(time).getHours()) < 10) {
      hours = `0${parseInt(new Date(time).getHours())}`;
    } else {
      hours = parseInt(new Date(time).getHours());
    }
    if (parseInt(new Date(time).getMinutes()) < 10) {
      minutes = `0${parseInt(new Date(time).getMinutes())}`;
    } else {
      minutes = parseInt(new Date(time).getMinutes());
    }
    return `${hours}:${minutes}`;
  };
  return (
    <SafeAreaView
      style={{
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        paddingVertical: hp(4),
      }}
    >
      {/* {console.log(value)} */}
      <View>
        <Text style={{ fontSize: 30 }}>Data</Text>
      </View>
      {/* {history && ( */}
      <FlatList
        data={value.reverse()}
        key={(item) => item.date}
        renderItem={({ item, index }) => (
          <View
            style={{
              height: hp("10"),
              width: wp(70),
              backgroundColor: "white",
              marginVertical: 5,
              borderWidth: 1,
              borderRadius: 10,
              elevation: 5,
              padding: hp(0.2),
            }}
          >
            {/* {console.log(item)} */}
            <Text style={{ fontSize: wp(4) }}>{Times(item.date)}</Text>
            <View style={{ flexDirection: "row" }}>
              <View
                style={{ marginVertical: wp(0.3), marginHorizontal: wp(1) }}
              >
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ fontSize: wp(3.7) }}>Nutrisi:</Text>
                  <Text style={{ fontSize: wp(3.7) }}>{item.nutirisi1}</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ fontSize: wp(3.7) }}>pH:</Text>
                  <Text style={{ fontSize: wp(3.7) }}>
                    {parseFloat(item.ph).toFixed(1)}
                  </Text>
                </View>
              </View>
              <View
                style={{ marginVertical: wp(0.3), marginHorizontal: wp(1) }}
              >
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ fontSize: wp(3.7) }}>Kelembaban:</Text>
                  <Text style={{ fontSize: wp(3.7) }}>{item.kelembaban}</Text>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ fontSize: wp(3.7) }}>Suhu:</Text>
                  <Text style={{ fontSize: wp(3.7) }}>{item.suhu}</Text>
                </View>
              </View>
              <View
                style={{ marginVertical: wp(0.3), marginHorizontal: wp(1) }}
              >
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ fontSize: wp(3.5) }}>Cahaya:</Text>
                  <Text style={{ fontSize: wp(3.5) }}>{item.cahaya}</Text>
                </View>
              </View>
            </View>
          </View>
        )}
      />
      {/* )} */}
      <TouchableOpacity
        style={{
          height: hp(8),
          width: wp(38),
          backgroundColor: "#008dee",
          elevation: 4,
          marginTop: hp(3),
          alignItems: "center",
          justifyContent: "center",
          borderRadius: hp(10),
        }}
        onPress={() => {
          dispatch_auth({
            type: "LOG_OFF",
          }),
            dispatch_listener({
              type: "OFF",
            });
          dispatch_listener({
            type: "VALUES",
          });
          Heartbeat.stopService();
        }}
      >
        <View>
          <Text style={{ fontSize: hp(4.5), color: "white" }}>KELUAR</Text>
        </View>
      </TouchableOpacity>
    </SafeAreaView>
  );
  // }
};

export default CustomDrawerContent;
