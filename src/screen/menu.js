import React, { useEffect, useContext, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  NativeModules,
  Modal,
  ImageBackground,
  TextInput,
} from "react-native";
import { connect } from "react-redux";
import Heartbeat from "../../Heartbeat";
import database from "@react-native-firebase/database";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { setHeartBeat, store } from "../../store";
import { ScrollView } from "react-native-gesture-handler";
import { listenerContext } from "../context/listener";
import { authContext } from "../context/authSlice";

const mainMenu = ({ navigation, heartBeat }) => {
  const { auth, dispatch_auth } = useContext(authContext);
  const { list, dispatch_listener } = useContext(listenerContext);
  const [Visible, setVisible] = useState(list.status ? false : true);
  const [json, setjson] = useState([]);
  const [tanaman, settanaman] = useState("");
  const [value, setValue] = useState({
    nutirisi1: "",
    "nilai ph": "",
    kelembaban: "",
    suhu: "",
    cahaya: "",
  });
  useEffect(() => {
    let data = database()
      .ref("tanaman")
      .on("value", function(snapshot) {
        settanaman(snapshot.val());
        if (tanaman) {
          data = database().ref(`${tanaman}`);
          data.on("child_added", function(snapshot) {
            let position = snapshot.val();
            if (position) {
              setValue(position);
            }
            // store.dispatch(setHeartBeat("anggrek"));
          });
        }
      });

    return () => {
      if (list.status) {
        let data = database().ref("tanaman");
        data.off();
      }
    };
  }, [list.status, tanaman]);

  useEffect(() => {
    let data = database().ref("jenis_tanaman/");
    data.on("child_added", (snapshot) => {
      json.push({
        id: snapshot.val().id,
        label: snapshot.val().label,
        value: snapshot.val().value,
        nutrisi_atas: snapshot.val().nutrisi_atas,
        nutrisi_bawah: snapshot.val().nutrisi_bawah,
        ph_atas: snapshot.val().ph_atas,
        ph_bawah: snapshot.val().ph_bawah,
      });
      setjson([...json]);
    });

    return () => {
      let data = database().ref("jenis_tanaman/");
      data.off();
    };
  }, []);

  const time = () => {
    let now = new Date();
    let jam = now.getHours();
    if (jam >= 5 && jam <= 11) {
      return "Pagi";
    } else if (jam > 11 && jam <= 18) {
      return "Siang";
    } else {
      return "Malam";
    }
  };

  const Times = () => {
    let hours = null;
    if (parseInt(new Date(value.dates).getHours()) + 4 === 24) {
      hours = "00";
    } else if (parseInt(new Date(value.dates).getHours()) + 4 > 24) {
      hours = parseInt(new Date(value.dates).getHours()) + 4 - 24;
    } else if (parseInt(new Date(value.dates).getHours()) + 4 < 10) {
      hours = `0${parseInt(new Date(value.dates).getHours()) + 4}`;
    } else {
      hours = parseInt(new Date(value.dates).getHours()) + 4;
    }
    return `${hours}:${new Date(value.dates).getMinutes()}`;
  };

  return (
    <View style={styles.container}>
      {/* {console.log(value)} */}
      <ImageBackground
        source={require("../asset/backgroundd.png")}
        style={{ flex: 1, resizeMode: "cover", justifyContent: "center" }}
      >
        <View
          style={{
            backgroundColor: "#008dee",
            height: hp(17),
            width: wp(100),
            borderBottomLeftRadius: 30,
            borderBottomRightRadius: 30,
            paddingHorizontal: wp(5),
            paddingVertical: hp(1),
          }}
        >
          <View
            style={{ flexDirection: "row", justifyContent: "space-between" }}
          >
            <View style={{ flexDirection: "column" }}>
              <View style={{ height: hp(6), width: hp(25) }}>
                <Image
                  source={require("../asset/icon.png")}
                  style={{ height: "100%", width: "100%" }}
                />
              </View>
              <Text
                style={{ marginTop: -5, color: "white", fontSize: hp(3.6) }}
              >
                {time()} {auth.name}
              </Text>
              <Text style={{ color: "white", fontSize: hp(2) }}>
                Cek Kondisi Tanaman Anda
              </Text>
            </View>
            <View
              style={{
                flexDirection: "row",
                alignContent: "center",
                alignItems: "center",
              }}
            >
              <View style={{ height: hp(10), width: hp(10) }}>
                <Image
                  source={require("../asset/boy.png")}
                  style={{ height: "100%", width: "100%" }}
                />
              </View>
              <TouchableOpacity
                style={{ height: hp(5), width: hp(3) }}
                onPress={() =>
                  // dispatch_auth({
                  //   type: "LOG_OFF",
                  // })
                  navigation.openDrawer()
                }
              >
                <Image
                  source={require("../asset/menu_bar.png")}
                  style={{ height: "100%", width: "100%" }}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <ScrollView>
          <View style={{ alignItems: "center" }}>
            <View
              style={{
                width: wp(80),
                backgroundColor: "#008dee",
                height: hp(5),
                marginTop: hp(2),
                borderRadius: 10,
                elevation: 5,
                flexDirection: "row",
                alignItems: "center",
                paddingHorizontal: wp(3),
                justifyContent: "center",
              }}
            >
              <Text style={{ color: "white", fontSize: hp(1.7) }}>
                Monitoring Tanaman {json[tanaman]?.label}
              </Text>
            </View>
            <View
              style={{
                borderWidth: 1,
                height: hp(9),
                width: wp(80),
                marginTop: hp(2),
                backgroundColor: "white",
                elevation: 4,
                borderRadius: 10,
                justifyContent: "center",
                paddingHorizontal: wp(3),
                justifyContent: "space-between",
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                }}
              >
                <View style={{ height: hp(5), width: hp(5) }}>
                  <Image
                    source={require("../asset/nutrition.png")}
                    style={{ height: "100%", width: "100%" }}
                  />
                </View>
                <Text style={{ fontSize: hp(2.4), marginLeft: wp(3) }}>
                  Nutrisi
                </Text>
              </View>
              <View style={{ flexDirection: "row", alignItems: "flex-end" }}>
                <Text
                  style={{ fontSize: hp(3), color: "red", marginRight: wp(1) }}
                >
                  {value.nutirisi1}
                </Text>
                <View style={{ alignItems: "flex-start", width: wp(8) }}>
                  <Text style={{ fontSize: hp(2) }}>PPM</Text>
                </View>
              </View>
            </View>
            <View
              style={{
                borderWidth: 1,
                height: hp(9),
                width: wp(80),
                marginTop: hp(2),
                backgroundColor: "white",
                elevation: 4,
                borderRadius: 10,
                justifyContent: "center",
                paddingHorizontal: wp(3),
                justifyContent: "space-between",
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                }}
              >
                <View style={{ height: hp(5), width: hp(5) }}>
                  <Image
                    source={require("../asset/Ph.png")}
                    style={{ height: "100%", width: "100%" }}
                  />
                </View>
                <Text style={{ fontSize: hp(2.4), marginLeft: wp(3) }}>pH</Text>
              </View>
              <View style={{ flexDirection: "row", alignItems: "flex-end" }}>
                <Text
                  style={{ fontSize: hp(3), color: "red", marginRight: wp(1) }}
                >
                  {parseFloat(value["nilai ph"]).toFixed(1)}
                </Text>
                <View style={{ alignItems: "flex-start", width: wp(8) }}>
                  <Text style={{ fontSize: hp(2) }}>pH</Text>
                </View>
              </View>
            </View>
            <View
              style={{
                borderWidth: 1,
                height: hp(9),
                width: wp(80),
                marginTop: hp(2),
                backgroundColor: "white",
                elevation: 4,
                borderRadius: 10,
                justifyContent: "center",
                paddingHorizontal: wp(3),
                justifyContent: "space-between",
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                }}
              >
                <View style={{ height: hp(5), width: hp(5) }}>
                  <Image
                    source={require("../asset/kelembaban.png")}
                    style={{ height: "100%", width: "100%" }}
                  />
                </View>
                <Text style={{ fontSize: hp(2.4), marginLeft: wp(3) }}>
                  kelembaban
                </Text>
              </View>
              <View style={{ flexDirection: "row", alignItems: "flex-end" }}>
                <Text
                  style={{ fontSize: hp(3), color: "red", marginRight: wp(1) }}
                >
                  {value.kelembaban}
                </Text>
                <View style={{ alignItems: "flex-start", width: wp(8) }}>
                  <Text style={{ fontSize: hp(2) }}>%</Text>
                </View>
              </View>
            </View>
            <View
              style={{
                borderWidth: 1,
                height: hp(9),
                width: wp(80),
                marginTop: hp(2),
                backgroundColor: "white",
                elevation: 4,
                borderRadius: 10,
                justifyContent: "center",
                paddingHorizontal: wp(3),
                justifyContent: "space-between",
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                }}
              >
                <View style={{ height: hp(5), width: hp(5) }}>
                  <Image
                    source={require("../asset/thermometer.png")}
                    style={{ height: "100%", width: "100%" }}
                  />
                </View>
                <Text style={{ fontSize: hp(2.4), marginLeft: wp(3) }}>
                  Temperatur
                </Text>
              </View>
              <View style={{ flexDirection: "row", alignItems: "flex-end" }}>
                <Text
                  style={{ fontSize: hp(3), color: "red", marginRight: wp(1) }}
                >
                  {value.suhu}
                </Text>
                {/* <View style={{ alignItems: "flex-start", width: wp(8) }}>
              <Text style={{ fontSize: hp(2) }}>PPM</Text>
            </View> */}
                <View
                  style={{
                    flexDirection: "row",
                    // alignItems: "flex-end",
                    justifyContent: "flex-start",
                    width: wp(8),
                  }}
                >
                  <Text style={{ fontSize: hp(1) }}>o</Text>
                  <Text style={{ fontSize: hp(2) }}>C</Text>
                </View>
              </View>
            </View>
            <View
              style={{
                borderWidth: 1,
                height: hp(9),
                width: wp(80),
                marginTop: hp(2),
                backgroundColor: "white",
                elevation: 4,
                borderRadius: 10,
                justifyContent: "center",
                paddingHorizontal: wp(3),
                justifyContent: "space-between",
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                }}
              >
                <View style={{ height: hp(5), width: hp(5) }}>
                  <Image
                    source={require("../asset/cahaya.png")}
                    style={{ height: "100%", width: "100%" }}
                  />
                </View>
                <Text style={{ fontSize: hp(2.4), marginLeft: wp(3) }}>
                  Intensitas Cahaya
                </Text>
              </View>
              <View style={{ flexDirection: "row", alignItems: "flex-end" }}>
                <Text
                  style={{ fontSize: hp(3), color: "red", marginRight: wp(1) }}
                >
                  {value.cahaya}
                </Text>
                <View style={{ alignItems: "flex-start", width: wp(8) }}>
                  <Text style={{ fontSize: hp(2) }}>lm</Text>
                </View>
              </View>
            </View>
            <TouchableOpacity
              style={{
                height: hp(8),
                width: wp(38),
                backgroundColor: "#008dee",
                elevation: 4,
                marginTop: hp(3),
                alignItems: "center",
                justifyContent: "center",
                borderRadius: hp(10),
              }}
              onPress={() => {
                list.status
                  ? (Heartbeat.stopService(),
                    dispatch_listener({
                      type: "OFF",
                    }))
                  : (Heartbeat.startService(),
                    dispatch_listener({
                      type: "ON",
                    }));
              }}
            >
              <View>
                <Text style={{ fontSize: hp(4.5), color: "white" }}>
                  {list.status ? "Stop" : "Start"}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "white",
    alignItems: "center",
  },
  view: {
    flex: 0.5,
    justifyContent: "center",
    alignItems: "center",
  },
  button: {
    backgroundColor: "gray",
    padding: 10,
    margin: 10,
  },
  text: {
    fontSize: 20,
    color: "white",
  },
});

const mapStateToProps = (store) => ({
  heartBeat: store.App.heartBeat,
});

export default connect(mapStateToProps)(mainMenu);
