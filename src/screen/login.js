import React, { useEffect, useContext, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  NativeModules,
  ImageBackground,
  TextInput,
  Modal,
} from "react-native";
import { connect } from "react-redux";
import Heartbeat from "../../../rn-heartbeat/Heartbeat";
import database from "@react-native-firebase/database";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { ScrollView } from "react-native-gesture-handler";
import { listenerContext } from "../../../rn-heartbeat/src/context/listener";
import { authContext } from "../../../rn-heartbeat/src/context/authSlice";

const Login = ({ navigation, heartBeat }) => {
  const { auth, dispatch_auth } = useContext(authContext);
  const { list, dispatch_listener } = useContext(listenerContext);
  const [Username, setUsername] = useState("");
  const [Password, setPassword] = useState("");
  const [Visible, setVisible] = useState(false);
  const [data, setdata] = useState("");
  console.log(auth);

  const login = () => {
    if (Username && Password) {
      let index = auth.data?.filter(function(obj) {
        return (
          obj.name.toLowerCase() === Username.toLowerCase() &&
          obj.password === Password
        );
      });
      console.log("index", index);
      if (index.length > 0) {
        dispatch_auth({
          type: "LOGIN",
          name: Username.toUpperCase(),
        });
        dispatch_listener({
          type: "VALUE",
          value: index[0].label,
        });
        database()
          .ref("tanaman")
          .set(`${index[0].id}`);
      } else {
        alert("Username atau Password Salah");
      }
    } else {
      alert("Username atau Password Tidak Boleh Kosong");
    }
    // auth.data.some;
  };

  const forget = () => {
    if (Username) {
      let index = auth.data?.filter(function(obj) {
        return obj.name.toLowerCase() === Username.toLowerCase();
      });
      // console.log("index", index);
      if (index.length > 0) {
        let New = auth.data;
        for (let i = 0; i < New.length; i++) {
          if (New[i].name.toLowerCase() === Username) {
            New[i].password = Password;
            console.log(New);
          }
        }
        dispatch_auth({
          type: "FORGET_PASSWORD",
          data: New,
        });
        alert("Password Berhasil Di Ganti");
        setVisible(false);
        setUsername("");
        setPassword("");
      } else {
        alert("Username Belum Terdaftar");
      }
    } else {
      alert("Username Tidak Boleh Kosong");
    }
    // auth.data.some;
  };

  return (
    <View style={styles.container}>
      <View style={{ justifyContent: "center", alignItems: "center" }}>
        <View style={{ height: wp(12), width: wp(80) }}>
          <Image
            source={require("../asset/icon.png")}
            style={{ height: "100%", width: "100%" }}
          />
        </View>
        <View
          style={{
            height: 50,
            width: wp(80),
            marginTop: hp(3),
            borderWidth: 1,
            borderRadius: 30,
            borderColor: "black",
            alignItems: "center",
            padding: 5,
            flexDirection: "row",
          }}
        >
          <View
            style={{
              height: 35,
              width: 35,
              position: "absolute",
              marginLeft: wp("2"),
            }}
          >
            <Image
              source={require("../asset/user1.png")}
              style={{ height: "100%", width: "100%" }}
            />
          </View>
          <View style={{ width: "100%", alignItems: "center" }}>
            <TextInput
              placeholder="Username"
              value={Username}
              onChangeText={setUsername}
            />
          </View>
        </View>
        <View
          style={{
            height: 50,
            width: wp(80),
            marginTop: hp(3),
            borderWidth: 1,
            borderRadius: 30,
            borderColor: "black",
            alignItems: "center",
            padding: 5,
            flexDirection: "row",
          }}
        >
          <View
            style={{
              height: 35,
              width: 35,
              position: "absolute",
              marginLeft: wp("2"),
            }}
          >
            <Image
              source={require("../asset/padlock.png")}
              style={{ height: "100%", width: "100%" }}
            />
          </View>
          <View style={{ width: "100%", alignItems: "center" }}>
            <TextInput
              placeholder="Password"
              secureTextEntry
              value={Password}
              onChangeText={setPassword}
            />
          </View>
        </View>
        <TouchableOpacity
          style={{
            height: 40,
            width: wp(33),
            backgroundColor: "#008dee",
            justifyContent: "center",
            alignItems: "center",
            borderRadius: 30,
            marginTop: hp(5),
          }}
          onPress={() => login()}
        >
          <Text style={{ color: "white", fontWeight: "bold", fontSize: 17 }}>
            LOGIN
          </Text>
        </TouchableOpacity>
      </View>
      <TouchableOpacity
        style={{
          flex: 0.4,
          alignItems: "flex-end",
          padding: wp(3),
          borderRadius: 30,
          marginVertical: hp(1),
        }}
        onPress={() => {
          setVisible(true), setPassword("");
        }}
      >
        <Text style={{ color: "grey" }}>Forget Password</Text>
      </TouchableOpacity>
      <Modal
        animationType="slide"
        transparent={true}
        visible={Visible}
        style={{
          alignItems: "center",
          justifyContent: "center",
          // padding: 20,
        }}
      >
        <View style={{ alignItems: "center", justifyContent: "center" }}>
          <View
            style={{
              height: hp(70),
              width: wp(80),
              marginTop: hp(5),
              backgroundColor: "#008dee",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <View>
              <Text
                style={{ fontSize: 20, color: "white", fontWeight: "bold" }}
              >
                Tulis Username Anda
              </Text>
            </View>
            <View
              style={{
                height: 50,
                width: wp(50),
                marginTop: hp(3),
                backgroundColor: "white",
                borderWidth: 1,
                borderRadius: 30,
                borderColor: "black",
                alignItems: "center",
                padding: 5,
                flexDirection: "row",
              }}
            >
              <View
                style={{
                  height: 35,
                  width: 35,
                  position: "absolute",
                  marginLeft: wp("2"),
                }}
              >
                <Image
                  source={require("../asset/user1.png")}
                  style={{ height: "100%", width: "100%" }}
                />
              </View>
              <View style={{ width: "100%", alignItems: "center" }}>
                <TextInput
                  placeholder="Username"
                  value={Username}
                  onChangeText={setUsername}
                />
              </View>
            </View>
            <View
              style={{
                height: 50,
                width: wp(50),
                marginTop: hp(3),
                backgroundColor: "white",
                borderWidth: 1,
                borderRadius: 30,
                borderColor: "black",
                alignItems: "center",
                padding: 5,
                flexDirection: "row",
              }}
            >
              <View
                style={{
                  height: 35,
                  width: 35,
                  position: "absolute",
                  marginLeft: wp("2"),
                }}
              >
                <Image
                  source={require("../asset/padlock.png")}
                  style={{ height: "100%", width: "100%" }}
                />
              </View>
              <View style={{ width: "100%", alignItems: "center" }}>
                <TextInput
                  placeholder="Password"
                  value={Password}
                  onChangeText={setPassword}
                  secureTextEntry
                />
              </View>
            </View>
            <TouchableOpacity
              style={{
                height: 40,
                width: wp(40),
                backgroundColor: "#ccf5ff",
                justifyContent: "center",
                alignItems: "center",
                borderRadius: 30,
                marginTop: hp(5),
                elevation: 7,
              }}
              onPress={() => forget()}
            >
              <Text
                style={{ color: "black", fontWeight: "bold", fontSize: 17 }}
              >
                Ubah Password
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                height: 40,
                width: wp(40),
                backgroundColor: "#ccf5ff",
                justifyContent: "center",
                alignItems: "center",
                borderRadius: 30,
                marginTop: hp(5),
                elevation: 7,
              }}
              onPress={() => setVisible(false)}
            >
              <Text
                style={{ color: "black", fontWeight: "bold", fontSize: 17 }}
              >
                Batal
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "white",
    justifyContent: "center",
    paddingVertical: hp(23),
  },
  view: {
    flex: 0.5,
    justifyContent: "center",
    alignItems: "center",
  },
  button: {
    backgroundColor: "gray",
    padding: 10,
    margin: 10,
  },
  text: {
    fontSize: 20,
    color: "white",
  },
});

const mapStateToProps = (store) => ({
  heartBeat: store.App.heartBeat,
});

export default connect(mapStateToProps)(Login);
