import React, { useContext, useEffect } from "react";
import { Image, View, StatusBar, Button, Text } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { SafeAreaProvider } from "react-native-safe-area-context";
import mainMenu from "../screen/menu";
import homeLogin from "../screen/homeLogin";
import register from "../screen/register";
import choice from "../screen/choice";
import login from "../screen/login";
import { authContext } from "../context/authSlice";
import { listenerContext } from "../context/listener";
import { createDrawerNavigator } from "@react-navigation/drawer";
import CustomDrawerContent from "../screen/CustomDrawerContent";

const StackApp = createStackNavigator();

const navOptionHandler = () => ({
  headerShown: false,
  tabBarOptions: { activeTintColor: "#F9BA9B" },
  animationEnabled: "false",
});

const Drawer = createDrawerNavigator();

function drawer({ navigation }) {
  const { list, dispatch_listener } = useContext(listenerContext);
  console.log(list);
  return (
    <Drawer.Navigator
      drawerContent={() => <CustomDrawerContent navigation={navigation} />}
    >
      {/* <Drawer.Screen name="Home" component={HomeScreen} /> */}
      {list.value ? (
        <Drawer.Screen
          name="Menu"
          component={mainMenu}
          options={navOptionHandler}
        />
      ) : (
        <Drawer.Screen
          name="Choice"
          component={choice}
          options={navOptionHandler}
        />
      )}
    </Drawer.Navigator>
  );
}

export default function MainNavigator() {
  const { auth, dispatch_auth } = useContext(authContext);

  return (
    <SafeAreaProvider>
      <NavigationContainer>
        <StackApp.Navigator>
          {!auth.isLogin ? (
            <>
              <StackApp.Screen
                name="homeLogin"
                component={homeLogin}
                options={navOptionHandler}
              />
              <StackApp.Screen
                name="register"
                component={register}
                options={navOptionHandler}
              />
              <StackApp.Screen
                name="login"
                component={login}
                options={navOptionHandler}
              />
            </>
          ) : (
            <StackApp.Screen
              name="mainMenu"
              component={drawer}
              options={navOptionHandler}
            />
          )}
        </StackApp.Navigator>
      </NavigationContainer>
    </SafeAreaProvider>
  );
}
